# shopt configuration
shopt -s globstar

PS1='\e[2m\w:\e[m\$ '

# aliases
alias l='ls -lh --color=auto'
alias ll='ls -Alh --color=auto'

#################
# Quick Scripts #
#################

# File naming convention rule enforcer (complete-rename)
crename() {
  rename 'y/A-Z/a-z/' ** \
  && rename 's/\.(?=.*\.)/-/g' ** \
  && rename 's/[\ _]/-/g' ** \
  && rename 's/-{2,}/-/g' ** \
  && rename 's/&/and/g' ** \
  && rename 's/…//g' ** \
  && rename 's/[\(\)\[\]\{\}?!;:,'\'\"\`\´\“\”']//g' ** \
  && rename 's/Á/á/g' **
}

genplay() {
  cp /home/thomas/.config/cmus/playlists/* /home/thomas/audio/playlists/ ;
  rename "s/$/.m3u/g" /home/thomas/audio/playlists/* ;
  find /home/thomas/audio/playlists/ -type f -exec sed -i 's/\/home\/thomas\/audio/\.\./g' {} \;
}

backup() {
  echo "";
  echo $'\e[1;96m'Back up all the things!$'\e[0m';
  echo "";
  sudo rsync -rlptgoDHv --exclude '*dosdevices' /home/thomas/ /media/thomas/backup/ --exclude=*.swp --progress;
}

# remap capslock to escape for vim
setxkbmap -option caps:escape

# rust environment variables
. "$HOME/.cargo/env"
